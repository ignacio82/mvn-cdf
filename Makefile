#makefile for testing makefiles
#built off of really good example from http://www.webalice.it/o.drofa/davide/makefile-fortran/makefile-fortran.html

#first create global variables

FC = mpif90
WRAPPER = gfortran
#WRAPPER = ifort
#wrapper states the underlying compiler for mpi

FCFLAGS =
LDFLAGS =
PROGRAMS =

#ifort options
ifeq ($(WRAPPER), ifort)

FCFLAGS += -O2
FCFLAGS += -error-limit 3
FCFLAGS += -implicitnone
#FCFLAGS += -check all
#FCFLAGS += -openmp
#FCFLAGS +=  -p

endif


#gfortan flags
ifeq ($(WRAPPER), gfortran)

#FCFLAGS += -fcheck=all
FCFLAGS += -fmax-errors=3
FCFLAGS += -fimplicit-none
#FCFLAGS += -fpack-derived
FCFLAGS += -O3
#FCFLAGS += -fcheck=bounds
FCFLAGS += -pg
#FCFLAGS += -fopenmp
#FCFLAGS += -I/usr/include
endif


LDFLAGS += -llapack -lblas
PROGRAMS += mvn_cdf_test_acc


# "make" builds all
all: $(PROGRAMS)


#following is for the dependencies of the object files
stats.o :: lapack.o mvt_genz.o

#rule for the program
mvn_cdf_test_acc.o :: stats.o lapack.o mvt_genz.o 
mvn_cdf_test_acc :: stats.o lapack.o mvt_genz.o 

# General rule for building prog from prog.o; $^ (GNU extension) is
# used in order to list additional object files on which the
# executable depends
%: %.o
	$(FC) $(FCFLAGS) -o $@ $^ $(LDFLAGS)

# General rules for building prog.o from prog.f90 or prog.F90; $< is
# used in order to list only the first prerequisite (the source file)
# and not the additional prerequisites such as module or include files
%.o: %.f90
	$(FC) $(FCFLAGS) -c $<

%.o: %.F90
	$(FC) $(FCFLAGS) -c $<

%.o: %.f
	$(FC) $(FCFLAGS) -c $<

%.o: %.F
	$(FC) $(FCFLAGS) -c $<

# Utility targets
.PHONY: clean veryclean

clean:
	rm -f *.o *.m *.mod

veryclean: clean
	rm -f *~ $(PROGRAMS)

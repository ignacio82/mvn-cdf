program mvn_cdf_test_acc

use normal_mod
use mvn_cdf_mod
use lapack_mod
   logical                               :: pd_logical
   real*8                                :: CDF
   integer                               :: i, d 
   real*8, dimension(:), allocatable     :: b
   real*8, dimension(:,:), allocatable   :: corr, chol
   
   mvn_fast_switch = .false.
!set up the correlation matrix and the limits of integration
   call sparse_grid_setup

! 1 dimension
d = 1
ALLOCATE (b(d))
ALLOCATE (corr(d,d))
ALLOCATE (chol(d,d))
! correlation matrix
corr(1,1) = 1.0d0
! limits of integration
b = 0.7d0
!now normalize
   call sigma_normalize(d,corr,b_inout_opt = b)
   call chol_decomp_sub(d, corr, chol, pd_logical )
   if ( pd_logical .eqv. .false. ) then
      print *, "the correlation matrix wasn't positive deninite, stopping"
   end if
   print *, "d", d
   print *, " "
   print *, "limits of integeration", b
   print *, " "
   print *, "correlation matrix"
   do i = 1, d
      print *, corr(i,1:i)
   end do
   print *, " "   
   CDF = mvn_cdf_multi(d, b, corr, chol)
   print *, "cdf eval with main algorithm", CDF   
   
DEALLOCATE(b)
DEALLOCATE(corr)
DEALLOCATE(chol)

! 2 dimensions
d = 2
ALLOCATE (b(d))
ALLOCATE (corr(d,d))
ALLOCATE (chol(d,d))
! correlation matrix
corr(1,1) = 1.0d0
corr(2,2) = 1.0d0
corr(1,2) = 0.5d0
corr(2,1) = 0.5d0
! limits of integration
b(1) = 0.7d0
b(2) = 0.5d0
!now normalize
   call sigma_normalize(d,corr,b_inout_opt = b)
   call chol_decomp_sub(d, corr, chol, pd_logical )
   if ( pd_logical .eqv. .false. ) then
      print *, "the correlation matrix wasn't positive deninite, stopping"
   end if
   print *, "d", d
   print *, " "
   print *, "limits of integeration", b
   print *, " "
   print *, "correlation matrix"
   do i = 1, d
      print *, corr(i,1:i)
   end do
   print *, " "   
   CDF = mvn_cdf_multi(d, b, corr, chol)
   print *, "cdf eval with main algorithm", CDF    
DEALLOCATE(b)
DEALLOCATE(corr)
DEALLOCATE(chol)
end program mvn_cdf_test_acc

module lapack_mod
   !contains subroutine I use to call routines from LAPACK(Linear Algebra PACKage)
   !module useful since these routines are a bit difficult to understand at first, so these subroutine I make
      !are more user friendly and easier to read in code

integer :: info !for error codes releases by these routines
logical, parameter :: print_info = .true. !controls whether to print any errors thrown by the lapack routines

contains

subroutine chol_decomp_sub(d_in,sigma_in,chol_decomp_out,pd_out_opt)
   !gets the choleksy decomp using LAPACK routines
   !I have it set to return the lower triangle
   integer, intent(in) :: d_in
   real*8, dimension(d_in,d_in),intent(in) :: sigma_in !this can either be the full matrix or just the lower triangle.
   real*8, dimension(d_in,d_in),intent(out) :: chol_decomp_out
   logical, intent(out), optional :: pd_out_opt !says whether the covariance matrix was pd

   integer :: i,j

   chol_decomp_out=0
   do i=1,d_in
      do j=1,i
         chol_decomp_out(i,j)=sigma_in(i,j)
      end do
   end do

   call dpotrf('L',d_in,chol_decomp_out,d_in,info) !calculates chol decomposition

   if ((info .ne. 0) .and. print_info) print *, "error with chol decomposition, error code:", info

   if (present(pd_out_opt)) then
      if (info .eq. 0) then
         pd_out_opt = .true.
      else
         pd_out_opt = .false.
      end if
   end if


end subroutine chol_decomp_sub

subroutine inv_chol_decomp_sub(d_in, chol_decomp_in, inv_chol_decomp_out)
   !given the cholesky decomp of a matrix, this function returns the inverse of the matrix
   integer, intent(in) :: d_in
   real*8, dimension(d_in,d_in), intent(in) :: chol_decomp_in
   real*8, dimension(d_in,d_in), intent(out) :: inv_chol_decomp_out

   integer :: i,j

   inv_chol_decomp_out = chol_decomp_in

   call dpotri('L',d_in,inv_chol_decomp_out,d_in,info) !calculates inverse using chol decomposition
   if ((info .ne. 0) .and. print_info) print *, "error with inverse of chol decomposition, error code:", info

   !now duplicate the lower triange part to the upper triangle part to generate the full symmetric matrix
   do i=2,d_in
      do j=1,i-1
         inv_chol_decomp_out(j,i) = inv_chol_decomp_out(i,j)
      end do
   end do


end subroutine inv_chol_decomp_sub


!haven't tested this subroutine yet
subroutine inv_pd_matrix_sub(d_in, sigma_in, inv_sigma_out, pd_out_opt)
   !gets the inverse of a positive definite matrix using the Cholesky decomposition

   integer, intent(in) :: d_in
   real*8, dimension(d_in,d_in),intent(in) :: sigma_in !this can either be the full matrix or just the lower triangle.
   real*8, dimension(d_in,d_in), intent(out) :: inv_sigma_out
   logical, intent(out), optional :: pd_out_opt !says whether the covariance matrix was pd

   real*8, dimension(d_in,d_in) :: chol_temp


   call chol_decomp_sub(d_in,sigma_in,chol_temp)

   !get error code if it wasn't positive definite
   if (present(pd_out_opt)) then
      if (info .eq. 0) then
         pd_out_opt = .true.
      else
         pd_out_opt = .false.
      end if
   end if

   call inv_chol_decomp_sub(d_in, chol_temp, inv_sigma_out)


end subroutine inv_pd_matrix_sub


subroutine qr_decomp_sub(m,n,a,q,r)
   !gets the qr decomposition of the m by n matrix a

   integer,intent(in) :: m !num rows
   integer,intent(in) :: n !num column
   real*8, dimension(m,n), intent(in) :: a
   real*8, dimension(m,n), intent(out) :: q
   real*8, dimension(n,n), intent(out) :: r

   integer  :: k    ! = n
   integer  :: lda    !=m
   real*8, dimension(n,n) :: tau
   real*8, dimension(:), allocatable :: work
   integer  :: lwork

   integer :: i, j


   !initalize some key variables
   k   = n
   lda = m

   !deterimine size of lwork
   lwork = -1
   allocate(work(1))
   call DGEQRF( M, N, A, LDA, TAU, WORK, LWORK, INFO )
   if ((info .ne. 0) .and. print_info) print *, "error with gettting size of lwork, error code=", info
   lwork = work(1)
   deallocate(work)
   allocate(work(lwork))

   !first subroutine changes the matrix a_temp into one part that contains r and another part that contains elements
      !that are latter used to construct q
   q = a
   call DGEQRF( M, N, q, LDA, TAU, WORK, LWORK, INFO )
   if ((info .ne. 0) .and. print_info) print *, "error with getting r in the qr decomposition, error code =", info


   !get r, which is on the upper triangle of a
   r=0
   do j=1,n
      do i=1,j
         r(i,j)=q(i,j)
      end do
   end do

   !this subroutine gets q
   call DORGQR( M, N, K, q, LDA, TAU, WORK, LWORK, INFO )
   if ((info .ne. 0) .and. print_info) print *, "error with getting q in the qr decomposition, error code =", info


end subroutine qr_decomp_sub


subroutine r_inv_sub(n,r,r_inv_out)
   !gets the inverse of r from the qr decomposition
   integer, intent(in) :: n
   real*8, dimension(n,n),intent(in) :: r
   real*8, dimension(n,n),intent(out) :: r_inv_out

   real*8, dimension(n*(n+1)/2) :: r_vector !stores either r or r_inv in vector format columwise
   integer :: i, j, jc

   !following code to convert to vector format gotten from the DTPTRI comments
   jc=1
   do j = 1,n
      do i=1,j
         r_vector(jc+i-1) = r(i,j)
      end do
      jc = jc + j
   end do

   call DTPTRI( 'U', 'N', n, r_vector, info )
   if ((info .ne. 0) .and. print_info) print *, "error with getting inverse of r, error code:", info

   r_inv_out=0
   jc=1
   do j = 1,n
      do i=1,j
         r_inv_out(i,j) = r_vector(jc+i-1)
      end do
      jc = jc + j
   end do



end subroutine r_inv_sub

end module lapack_mod


!module contains various statistical functions and subroutines related to the extreme value and normal distributions as well as estimating means and
   !covariances


!haven't tested this model yet
module uniform_mod

contains

function r_uniform(a,b)
   !draws a random uniform between intervals a to b
   real*8, intent(in) :: a, b
   real*8 :: r_uniform

   call random_number(r_uniform)
   r_uniform = a +(b-a)*r_uniform

end function r_uniform

end module


module logit_mod
implicit none

  real*8, parameter :: min_exp_arg=-7E2 !if the argument for the exponent is less than this value, then there will
      !be underflow problems
   real*8, parameter :: max_exp_arg=7E2 !if the argument for the exponent is greater than this value, then there will be overflow problems


contains


function emax_ev(num_choices,vals_in)
   !computes the emax for the iid extreme value distribution
   !do a transformation where within the log, I multiple and divide by the exp of the largest
      !choice-specific value function.  This prevents overflow with the exponentation function since
      !now all the exp args will be less than one

   integer, intent(in) :: num_choices
   real*8, dimension(num_choices), intent(in) :: vals_in
   real*8 :: emax_ev

   real*8 :: max_val
   real*8, dimension(num_choices) :: vals_transform
   integer :: i

   emax_ev=0
   max_val = maxval(vals_in)
   vals_transform = vals_in - max_val

   do i=1,num_choices
      if (vals_transform(i) > min_exp_arg) then
         emax_ev = emax_ev + exp( vals_transform(i) )
      end if
   end do

   emax_ev = max_val + log(emax_ev)

end function emax_ev


function logit_prob(x)
   real*8, intent(in) :: x
   real*8 :: logit_prob

  ! logit_prob = 1/(1+exp(-x))


   !now get the joboffer_probabilities
   logit_prob = exp(x)
   logit_prob = logit_prob/(1+ logit_prob)


end function logit_prob

function inv_logit_prob(x)
   real*8, intent(in) :: x
   real*8 :: inv_logit_prob

   inv_logit_prob = -log( (1.0/ x ) - 1 )

end function

real function inv_ev(p)

   implicit none
   real*8, intent(in) :: p
   if (p<.0000001) then
      inv_ev=-3.0
   else if (p>1-.0000001) then
      inv_ev=16.0
   else
      inv_ev=-log(-log(p))
   end if

end function


function mlogit_prob(num_choices,choice_index,vals_in)
   !computes the choice probabilities for the iid extreme value distribution (i.e. multinomial choice probabilities)
   !do a transformation in which I multiple and divide by the exp of the largest
      !choice-specific value function.  This prevents overflow with the exponentation function since
      !now all the exp args will be less than one

   integer, intent(in) :: num_choices
   integer, intent(in) :: choice_index !indicates the index of the chosen choice
   real*8, dimension(num_choices), intent(in) :: vals_in
   real*8 :: mlogit_prob

   real*8 :: max_val
   real*8, dimension(num_choices) :: vals_transform, exp_vals_transform
   integer :: i

   mlogit_prob=0
   max_val = maxval(vals_in)
   vals_transform = vals_in - max_val

   do i=1,num_choices
      if (vals_transform(i) > min_exp_arg) then
         exp_vals_transform(i) = exp( vals_transform(i) )
      else
         exp_vals_transform(i) = 0
      end if
   end do

   mlogit_prob = exp_vals_transform(choice_index)/sum(exp_vals_transform)

end function mlogit_prob


function inv_ev_func(p) result(x)
   real*8, intent(in) :: p
   real*8 :: x

   !the inverse is -log(-log(p)).  Edit some things to make a bit more stable
   x = p
   if (x < tiny(x) ) x = tiny(x)

   x= -log(x)
   if (x < tiny(x) ) x = tiny(x)

   x= -log(x)

end function

function rand_ev_func()
   !draws a random extreme value
   real*8 :: rand_ev_func
   real*8 :: rand_uniform

   call random_number(rand_uniform)

   rand_ev_func = inv_ev_func(rand_uniform)



end function rand_ev_func


end module logit_mod

module sparse_grid_mod
   !module contains integration nodes for numerical integration on a sparse grid
   !Files are downloaded from http://www.sparse-grids.de/, the website by by Florian Heiss & Viktor Winschel.  Code is for their paper "Likelihood Approximation by Numerical Integration on Sparse Grids"  More detailed descriptions can be found on this website

   !accronymns
   !KPU = Kronrod-Patterson Uniform
   !GQU = Gaussian Quadrature Uniform
   !SQ = Sparse Grid

   !following 3 parameters controls how many files to import
   integer, parameter :: max_level_sg  = 10!maximum number for the accuracy level for sparse grid integration
   integer, parameter :: max_d_sg   = 4!maximum number of dimensions
   integer, parameter :: max_nodes_gqu  = 7!number of nodes for gaussian quadrature (univariate)

   character(len=30), parameter :: path_kpu_sg = './KPU/' !path were the weights are stores
   character(len=30), parameter :: path_gqu = './GQU_d1/' !path were the weights are stores

   type sg_type
      integer :: n_nodes !number of nodes
      real*8, dimension(:,:), allocatable :: nodes
      real*8, dimension(:), allocatable :: weights
   end type

   type gqu_type
      real*8, dimension(:), allocatable :: weights
      real*8, dimension(:), allocatable :: nodes
   end type

   type(sg_type), dimension( max_d_sg, max_level_sg) :: sg_obj !object for storing the above type
   type(gqu_type), dimension(max_nodes_gqu) :: gqu_obj !object for storing gaussian-hermite quadrature nodes


contains

subroutine sparse_grid_setup()
   !sets up the global object shown above
   integer :: d, l, n_nodes
   real*8, dimension(:,:), allocatable :: sg_matrix

   !do the main kpu matrix
   do d= 1, max_d_sg
      do l = 1, max_level_sg
         call sparse_grid_read('KPU',path_kpu_sg, d,l, sg_matrix, n_nodes )

         sg_obj(d,l) % n_nodes = n_nodes

         allocate( sg_obj(d,l) % weights(n_nodes) )
         allocate( sg_obj(d,l) % nodes(d,n_nodes) )

         sg_obj(d,l) % nodes(:,:) = sg_matrix(:d,:)
         sg_obj(d,l) % weights(:) = sg_matrix(d+1,:)

      end do
   end do

   !now quadrature points for gauss-hermite univariate quadrature
   do l =1, max_nodes_gqu
      call sparse_grid_read('GQU',path_gqu, 1,l, sg_matrix, n_nodes)

      allocate( gqu_obj(l) % weights(l) )
      allocate( gqu_obj(l) % nodes(l) )

      gqu_obj(l) % nodes(:) = sg_matrix(1,:)
      gqu_obj(l) % weights(:) = sg_matrix(2,:)

   end do


end subroutine



subroutine sparse_grid_read(type_grid, path_in, d,level,sparse_grid_out,n_nodes_out)
!Code for reading in weights and nodes.  Files are downloaded from http://www.sparse-grids.de/, the website by by Florian Heiss & Viktor Winschel.  Code is for their paper "Likelihood Approximation by Numerical Integration on Sparse Grids"  More detailed descriptions can be found on this website

!type: The type of grid that is being imported (KPU, GQU, etc)
!d:number of dimensions.  File has d+1 columns where last column is for the weights
!level: level of accuracy.  integration technique will exactly integrate polynomials of
!degree 2*level-1
!sparse_grid_out: The matrix of nodes and weights.  Note that the final matrix is the transpose of original file.  This is because Fortran stores data in column-major order, so storing it this way will make the integration routine run faster (since an entire column should be in cache)

!will spit out error and terminate program if the matrix of interest is not present


!IMPORTANT use / in file path.  If using Windows, this need to be changed.

   implicit none
   character(3), intent(in) :: type_grid
   character(len=30), intent(in) :: path_in
   integer,intent(in) :: d,level
   real*8, dimension(:,:),allocatable,intent(out) ::sparse_grid_out
   integer,intent(out) :: n_nodes_out
   integer :: i,j,ios, unit_int
   character(1) :: junk
   character(30) :: file_sparse_grid,format_string
   real*8, dimension(:,:),allocatable ::sparse_grid_temp
   logical ::file_exists

   !initialize string format which varies depending on size of d and level
   if (d < 10) then
      if (level<10) then
         format_string = "(A2,A3,A1,A3,A2,I1,A2,I1,A4)"
       else
         format_string = "(A2,A3,A1,A3,A2,I1,A2,I2,A4)"
       end if
   else
      if (level<10) then
         format_string = "(A2,A3,A1,A3,A2,I2,A2,I1,A4)"
      else
         format_string = "(A2,A3,A1,A3,A2,I2,A2,I2,A4)"
      end if
   endif

   write (file_sparse_grid,format_string) trim(path_in), type_grid, '/', type_grid, '_d',d,'_l',level,'.asc'
   file_sparse_grid=trim(file_sparse_grid)

   inquire(file=file_sparse_grid,exist=file_exists)

   if (file_exists .eqv. .false.) then
      print *, "sparse grid file", file_sparse_grid, "not found"
      print *, "can get these from http://www.sparse-grids.de/"
      stop
   end if



   !code for reading number of lines in file gotten from http://web.utah.edu/thorne/computing/Handy_Fortran_Tricks.pdf
   n_nodes_out = 0
   OPEN(newunit = unit_int, FILE=file_sparse_grid, status = "old" )
   DO J=1,100000
      READ(unit_int,*,IOSTAT=ios) junk
      IF (ios /= 0) EXIT
         IF (J == 100000) THEN
            write(*,*) 'Error:  Maximum number of records exceeded...'
            write(*,*) 'Exiting program now...'
         STOP
      ENDIF
      n_nodes_out = n_nodes_out + 1
   ENDDO
   REWIND(unit_int)

   !This deals with the sparse_grid_out possibly beign allocated already
  ! if (allocated(sparse_grid_temp)) deallocate(sparse_grid_temp)
  ! if (allocated(sparse_grid_out)) deallocate(sparse_grid_out)


   allocate(sparse_grid_temp(n_nodes_out,d+1))
   allocate(sparse_grid_out(d+1,n_nodes_out))

   do i=1,n_nodes_out
      read(unit_int,*)(sparse_grid_temp(i,j),j=1,d+1)
   end do
   close(unit_int)

   sparse_grid_out=transpose(sparse_grid_temp)

end subroutine  sparse_grid_read

end module sparse_grid_mod



module normal_mod
   !contains function and subroutines related to the normal distribution
   use lapack_mod

   real*8, parameter :: inv_sqrt_2pi=0.39894228040143270
   real*8, parameter :: ln_inv_sqrt_2pi=log(0.39894228040143270)
   real*8, parameter :: cdf_scalar = 1.0D+00/sqrt(2.0D+00) !used for calulating the cdf

   interface cdf_norm
      module procedure cdf_norm_one_arg
      module procedure cdf_norm_three_arg
   end interface

   interface pdf_norm
      module procedure pdf_norm_one_arg
      module procedure pdf_norm_three_arg
   end interface


contains

double precision function pdf_norm_one_arg(x) result(pdf_norm) !calcuates the normal pdf
   implicit none
   real*8,intent(in) :: x
   !note, hasn't been tested yet, but should be fine

   pdf_norm=inv_sqrt_2pi*exp( -(.5D+00*x*x) )

end function pdf_norm_one_arg

double precision function pdf_norm_three_arg(x,mu,sigma) result(pdf_norm)!calcuates the normal pdf
   implicit none
   real*8,intent(in) :: x
   real*8,intent(in) :: mu
   real*8,intent(in) :: sigma

   real*8 :: z_score, sigma_inv

   sigma_inv = 1.0/sigma
   z_score = (x-mu)*sigma_inv

   pdf_norm=inv_sqrt_2pi*sigma_inv*exp( -(.5D+00*z_score*z_score) )

end function pdf_norm_three_arg

double precision function bvn_pdf(x,y,rho) !bivariate normal pdf with mean of 0, variances of 1, and correlation coef. rho
   implicit none
   real*8,intent(in)::x,y,rho
   real*8, parameter :: inv_2pi=0.15915494309189535

   bvn_pdf=(inv_2pi/sqrt(1.0D+00-rho*rho) )*exp(-.5D+00*( (x*x+y*y-2.0D+00*rho*x*y)/(1.0D+00-rho*rho)) )

end function bvn_pdf

double precision function ln_pdf_norm(x,mu,sigma) !calcuates the natural log of the normal pdf
   implicit none
   real*8,intent(in) :: x
   real*8,intent(in) :: mu
   real*8,intent(in) :: sigma

   real*8 :: z_score, sigma_inv

   sigma_inv = 1.0/sigma
   z_score = (x-mu)*sigma_inv

   ln_pdf_norm=ln_inv_sqrt_2pi + log(sigma_inv) -(.5D+00*z_score*z_score)

end function ln_pdf_norm


!code gotten from http://jblevins.org/mirror/amiller/




function cdf_norm_one_arg(z_in) result(cdf_norm)
   !computes the standard normal cdf using fortran's intrinsic error function
   !modified from code I got from Chris Clapp
   real*8 :: cdf_norm
   real*8, intent(in) :: z_in
   real*8 :: z_scaled

   z_scaled = z_in * cdf_scalar

   if (z_scaled .le. 0) then
      cdf_norm = 1.0-( 0.5*erfc(z_scaled) )
   else
      cdf_norm = .5 * erfc(-z_scaled)
   end if

end function cdf_norm_one_arg

function cdf_norm_three_arg(x,mu,sigma) result(cdf_norm)
   !computes the standard normal cdf using fortran's intrinsic error function
   !modified from code I got from Chris Clapp
   real*8 :: cdf_norm
   real*8, intent(in) :: x, mu, sigma
   real*8 :: z,z_scaled

   z = ( x-mu)/sigma

   z_scaled = z * cdf_scalar

   if (z_scaled .le. 0) then
      cdf_norm = 1.0-( 0.5*erfc(z_scaled) )
   else
      cdf_norm = .5 * erfc(-z_scaled)
   end if

end function cdf_norm_three_arg





FUNCTION rnorm() RESULT( fn_val )

!   Generate a random normal deviate using the polar method.
!   Reference: Marsaglia,G. & Bray,T.A. 'A convenient method for generating
!              normal variables', Siam Rev., vol.6, 260-264, 1964.

IMPLICIT NONE
REAL  :: fn_val

! Local variables

REAL            :: u, sum
REAL, SAVE      :: v, sln
LOGICAL, SAVE   :: second = .FALSE.
REAL, PARAMETER :: one = 1.0, vsmall = TINY( one )

IF (second) THEN
! If second, use the second random number generated on last call

  second = .false.
  fn_val = v*sln

ELSE
! First call; generate a pair of random normals

  second = .true.
  DO
    CALL RANDOM_NUMBER( u )
    CALL RANDOM_NUMBER( v )
    u = SCALE( u, 1 ) - one
    v = SCALE( v, 1 ) - one
    sum = u*u + v*v + vsmall         ! vsmall added to prevent LOG(zero) / zero
    IF(sum < one) EXIT
  END DO
  sln = SQRT(- SCALE( LOG(sum), 1 ) / sum)
  fn_val = u*sln
END IF

RETURN
END FUNCTION rnorm



subroutine get_random_mvn(d, mean_in, cholesky_lt_in, rand_mvn_out)
   !drawns a vector of multivariate normal variables
   integer, intent(in) :: d !dimensions
   real*8, dimension(d), intent(in) :: mean_in
   real*8, dimension(d,d), intent(in) :: cholesky_lt_in !lower triangle (lt) cholesky decomp,  the upper triangle needs to be 0s
   real*8, dimension(d), intent(out) :: rand_mvn_out

   real*8, dimension(d) ::  rand_standard_normal
   integer :: i


   do i=1,d
      rand_standard_normal(i) = rnorm()
   end do


   rand_mvn_out=matmul(cholesky_lt_in,rand_standard_normal)

   rand_mvn_out = rand_mvn_out + mean_in



end subroutine get_random_mvn


function mvn_pdf(d,mean,sigma,x) result(pdf)!computes the mulitivariate normal pdf
   !warning, function hasn't been tested

   integer, intent(in) :: d
   real*8, dimension(d),intent(in) :: mean
   real*8, dimension(d,d),intent(in) :: sigma
   real*8, dimension(d),intent(in) :: x
   real*8 :: pdf

   real*8, parameter :: pi=3.1415926535897932384626433832795
   real*8, dimension(d) :: x_dif
   real*8, dimension(1,d) :: x_dif_rank2
   real*8, dimension(1) :: pdf_rank1
   real*8, dimension(d,d) :: sigma_chol,sigma_inv
   real*8 :: kernel,det
   integer :: i,j
   logical :: pd_logical

   !get deterimant by getting the cholesky decomposition
   call chol_decomp_sub(d, sigma, sigma_chol, pd_logical )
   if ( pd_logical .eqv. .false. ) then
      print *, "covariance matrix for mvn_pdf is not positive definite, stopping"
      stop
   end if

   det=1.0D+00
   do i=1,d
      det=det*sigma_chol(i,i)
   end do
   det=det*det

  ! print *, "det: ", det

   !now get inverse of sigma from the cholesky decomp
   call inv_chol_decomp_sub(d, sigma_chol, sigma_inv)


   x_dif=x-mean
   x_dif_rank2(1,:)=x-mean
   kernel=1.0D+00/( (2.0D+00*pi)**(dble(d)/2.0D+00)*sqrt(det))

   pdf_rank1=kernel*exp(-.5D+00*matmul(matmul(x_dif_rank2,sigma_inv),x_dif) )
   pdf=pdf_rank1(1)

end function mvn_pdf






!gotten from Alan Genz's webpage, function renamed for my purposes

FUNCTION inv_cdf_norm( P ) RESULT(PHINV)
!
!  ALGORITHM AS241  APPL. STATIST. (1988) VOL. 37, NO. 3
!
!  Produces the normal deviate Z corresponding to a given lower
!  tail area of P.

      INTEGER, PARAMETER   :: STND = SELECTED_REAL_KIND(12, 60)

!
      REAL(KIND=STND), INTENT(IN) :: P
      REAL(KIND=STND)             :: PHINV
!
      REAL(KIND=STND) :: Q, R
      REAL(KIND=STND), PARAMETER :: SPLIT1 = 0.425E0_STND, SPLIT2 = 5,      &
                           CONST1 = 0.180625E0_STND, CONST2 = 1.6E0_STND
!
!     Coefficients for P close to 0.5
!
      REAL(KIND=STND), PARAMETER ::            &
           A0 = 3.3871328727963666080E00_STND, &
           A1 = 1.3314166789178437745E+2_STND, &
           A2 = 1.9715909503065514427E+3_STND, &
           A3 = 1.3731693765509461125E+4_STND, &
           A4 = 4.5921953931549871457E+4_STND, &
           A5 = 6.7265770927008700853E+4_STND, &
           A6 = 3.3430575583588128105E+4_STND, &
           A7 = 2.5090809287301226727E+3_STND, &
           B1 = 4.2313330701600911252E+1_STND, &
           B2 = 6.8718700749205790830E+2_STND, &
           B3 = 5.3941960214247511077E+3_STND, &
           B4 = 2.1213794301586595867E+4_STND, &
           B5 = 3.9307895800092710610E+4_STND, &
           B6 = 2.8729085735721942674E+4_STND, &
           B7 = 5.2264952788528545610E+3_STND
!
!     Coefficients for P not close to 0, 0.5 or 1.
!
      REAL(KIND=STND), PARAMETER ::             &
           C0 = 1.42343711074968357734E00_STND, &
           C1 = 4.63033784615654529590E00_STND, &
           C2 = 5.76949722146069140550E00_STND, &
           C3 = 3.64784832476320460504E00_STND, &
           C4 = 1.27045825245236838258E00_STND, &
           C5 = 2.41780725177450611770E-1_STND, &
           C6 = 2.27238449892691845833E-2_STND, &
           C7 = 7.74545014278341407640E-4_STND, &
           D1 = 2.05319162663775882187E00_STND, &
           D2 = 1.67638483018380384940E00_STND, &
           D3 = 6.89767334985100004550E-1_STND, &
           D4 = 1.48103976427480074590E-1_STND, &
           D5 = 1.51986665636164571966E-2_STND, &
           D6 = 5.47593808499534494600E-4_STND, &
           D7 = 1.05075007164441684324E-9_STND
!
!  Coefficients for P near 0 or 1.
!
      REAL(KIND=STND), PARAMETER ::             &
           E0 = 6.65790464350110377720E00_STND, &
           E1 = 5.46378491116411436990E00_STND, &
           E2 = 1.78482653991729133580E00_STND, &
           E3 = 2.96560571828504891230E-1_STND, &
           E4 = 2.65321895265761230930E-2_STND, &
           E5 = 1.24266094738807843860E-3_STND, &
           E6 = 2.71155556874348757815E-5_STND, &
           E7 = 2.01033439929228813265E-7_STND, &
           F1 = 5.99832206555887937690E-1_STND, &
           F2 = 1.36929880922735805310E-1_STND, &
           F3 = 1.48753612908506148525E-2_STND, &
           F4 = 7.86869131145613259100E-4_STND, &
           F5 = 1.84631831751005468180E-5_STND, &
           F6 = 1.42151175831644588870E-7_STND, &
           F7 = 2.04426310338993978564E-15_STND
!
      Q = ( 2*P - 1 )/2
      IF ( ABS(Q) <= SPLIT1 ) THEN
         R = CONST1 - Q*Q
         PHINV = Q*( ( ( ((((A7*R + A6)*R + A5)*R + A4)*R + A3) &
                       *R + A2 )*R + A1 )*R + A0 )              &
                 /( ( ( ((((B7*R + B6)*R + B5)*R + B4)*R + B3)  &
                       *R + B2 )*R + B1 )*R + 1 )
      ELSE
         R = MIN( P, 1 - P )
         IF ( R > 0 ) THEN
            R = SQRT( -LOG(R) )
            IF ( R <= SPLIT2 ) THEN
               R = R - CONST2
               PHINV = ( ( ( ((((C7*R + C6)*R + C5)*R + C4)*R + C3) &
                           *R + C2 )*R + C1 )*R + C0 )              &
                     /( ( ( ((((D7*R + D6)*R + D5)*R + D4)*R + D3)  &
                            *R + D2 )*R + D1 )*R + 1 )
            ELSE
               R = R - SPLIT2
               PHINV = ( ( ( ((((E7*R + E6)*R + E5)*R + E4)*R + E3) &
                           *R + E2 )*R + E1 )*R + E0 )              &
                     /( ( ( ((((F7*R + F6)*R + F5)*R + F4)*R + F3)  &
                           *R + F2 )*R + F1 )*R + 1 )
            END IF
         ELSE
            PHINV = 9
         END IF
         IF ( Q < 0 ) THEN
            PHINV = - PHINV
         END IF
      END IF
!
END FUNCTION inv_cdf_norm


function rand_trunc_norm (mu, sigma, a, b) result(z)
   !draws z ~N(mu, sigma), with truncation points at a and b
   real*8 :: z
   real*8, intent(in) :: mu, sigma, a, b

   real*8 :: u, a_cdf, b_cdf, sigma_inv

   call random_number(u)
   sigma_inv = 1/sigma
   a_cdf = cdf_norm( (a-mu)*sigma_inv )
   b_cdf = cdf_norm( (b-mu)*sigma_inv )

   z = sigma * inv_cdf_norm( u* (b_cdf - a_cdf) + a_cdf ) + mu

end function rand_trunc_norm

function ln_pdf_trunc_norm (x, mu, sigma, a, b) result(pdf)
   !gets log of the truncated normal pdf
   real*8 :: pdf
   real*8, intent(in) :: x, mu, sigma, a, b

   pdf = ln_pdf_norm(x,mu,sigma) - log( cdf_norm(b, mu, sigma) - cdf_norm(a, mu, sigma) )


end function ln_pdf_trunc_norm





end module normal_mod

module mvn_cdf_mod
   !module for computing the multivariat normal cdf, P(X<b).  Some important Notes
      !1) the subroutine sparse_grid_setup needs to be called befor using most of these functions
      !2) The input matrix is either the correlation matrix or the cholesky decomposition of the cholesky matrix.  So in other words, the variances all need to be equal to one

   use normal_mod
   use sparse_grid_mod
   use mvstat, only : MVDIST, MVBVU, MVTVTL, max_iter_tri_cdf  !this module is in the mvt_genz.f90 file

   integer :: acc_level_mvn = 3 !controls the accuracy level for the sparse grid intergration in mvn_cdf_fast.  This determines how many quadrature nodes will be used.  A higher levels leads to higher accuaracy and more nodes used in the calculation.  Not making it a parameter so that it can be changes during run tim
   integer :: acc_level_tri = 2 ! analogous accuracy level for the tri-variate normal
   logical :: mvn_fast_switch = .true. !controls whether to use the fast mvn cdf  and trivariate normal cdf algorithms.  The fast algorithms are my own design which use non-adaptive integeration, the slow ones are the ones made by genz which adaptive integration.

   interface mvn_cdf_fast
      module procedure mvn_cdf_fast_less_args
      module procedure mvn_cdf_fast_more_args
   end interface

contains


subroutine sigma_normalize(d,sigma_inout,diag_vars_out_opt,b_inout_opt)  !normalized the diagonals of a covariance matrix to 1.  Also exports the inverse of the square root of the diagonals if needed.  limits of integration are also normalized if needed

   integer, intent(in) :: d
   real*8, intent(in out),dimension(d,d) :: sigma_inout
   real*8, dimension(d):: diag_vars
   real*8, dimension(d),intent(out),optional:: diag_vars_out_opt
   real*8, dimension(d),intent(in out), optional :: b_inout_opt
   integer :: i,j


   do i=1,d
      diag_vars(i)=1.0D+00/sqrt(sigma_inout(i,i))
   end do

   do i=1,d
      do j=1,d
         sigma_inout(i,j)=sigma_inout(i,j)*diag_vars(i)*diag_vars(j)
      end do
   end do

   if(present(diag_vars_out_opt)) diag_vars_out_opt=diag_vars
   if(present(b_inout_opt)) then
      do i=1,d
         b_inout_opt(i)=b_inout_opt(i)*diag_vars(i)
      end do
   end if

end subroutine sigma_normalize



function bvn_cdf( b_in, rho_in )
   !uses MVBVU to calculate the bivariate normal cdf.  Has double precision accuracy
   real*8, dimension(2), intent(in) :: b_in
   real*8, intent(in) :: rho_in !only need the correlation for this subroutine
   real*8 :: bvn_cdf


   bvn_cdf = MVBVU( -b_in(1), -b_in(2), rho_in )  !negatives because this function does P(X>b), which is equal to P( X< -b) since the normal dist. is symetric

end function

function tvn_cdf_fast(b_in,corr_in) result(tvn_cdf)
  !this is the 2nd plackett formula presented in Genz 2004 page 254 for the trivariate normal integral, which I evaluate with numerical integration
   !this should be a faster version because I don't do adaptive integration
   !IMPORTANT: need to input only the correlation matrix, so all the diagonals need to be one
   real*8 :: tvn_cdf
   real*8, dimension(3),intent(in) :: b_in
   real*8, dimension(3,3),intent(in) :: corr_in

   integer:: i,j


   real*8, parameter :: inv_2pi=0.15915494309189535
   real*8, dimension(3):: b

   real*8, dimension(3) :: inv_sigma
   real*8 :: rho12,rho13,rho23, b1, b2, b3
   real*8 :: t, common_denom, common_u2, common_u3, u2,u3,int_u2, int_u3,cdf_ref
   !the common_u2 term is 1-rho^2*t^2
   real*8, dimension(acc_level_tri) :: nodes, weights

!   b=b_in
!   sigma=sigma_in

   nodes = gqu_obj(acc_level_tri) % nodes
   weights = gqu_obj(acc_level_tri) % weights

 !  print *, "nodes", nodes
 !  print *, "weights", weights


   !first sorting to have largest correlation be sigma

   !if sigma 1,2 largest, swap variables 1 and 3
!   if ( (abs(sigma(1,2))>abs(sigma(1,3))) .and. (abs(sigma(1,2))>abs(sigma(2,3))) ) then
!      call variable_swap(3,b,sigma,1,3)
!   !if sigma 1,3 largest, then swap 1 and 2
!   else if  ( (abs(sigma(1,3))>abs(sigma(2,3))) ) then
!      call variable_swap(3,b,sigma,1,2)
!   end if

   !now uppack the arrays into scalars to make code eaiser to read and the help with speed

   b1 = b_in(1)
   b2 = b_in(2)
   b3 = b_in(3)

   rho12 = corr_in(2,1)
   rho13 = corr_in(3,1)
   rho23 = corr_in(3,2)

!   print *, "rhos", rho12, rho13, rho23
!   stop


   cdf_ref=cdf_norm(b1)*MVBVU(-b2,-b3,rho23)

!   print *, "cdf_ref", cdf_ref
!   stop


   int_u2=0.0D+00
   int_u3=0.0D+00


   do i=1,acc_level_tri
      t=nodes(i)
      common_u2=1.0D+00-rho13*rho13*t*t
      common_u3=1.0D+00-rho12*rho12*t*t
      common_denom=1.0D+00-rho12*rho12*t*t-rho13*rho13*t*t-rho23*rho23 &
         + 2.0D+00*t*t*rho13*rho12*rho23

  !    print *, "t and commons", t, common_u2, common_u3, common_denom
  !    stop

      u2=( b2*common_u2 -b1*t*(rho12-rho13*rho23)-b3*(rho23-rho13 &
         *rho12*t*t) )/sqrt(common_u2*common_denom)
      u3=( b3*common_u3 -b1*t*(rho13-rho12*rho23)-b2*(rho23-rho13 &
         *rho12*t*t) )/sqrt(common_u3*common_denom)

  !    print *, u2, u3

      int_u2=int_u2+weights(i)*cdf_norm(u2)*exp(-(b1*b1+b3*b3-2.0D+00*rho13*t*b1*b3) / &
         (2.0D+00*common_u2))/sqrt(common_u2)
      int_u3=int_u3+weights(i)*cdf_norm(u3)*exp(-(b1*b1+b2*b2-2.0D+00*rho12*t*b1*b2) / &
         (2.0D+00*common_u3))/sqrt(common_u3)

  !    print *, "int_u2", int_u2, "int_u3", int_u3

  !    stop

   end do

   tvn_cdf=cdf_ref+inv_2pi*(rho12*int_u3+rho13*int_u2)

   contains
   subroutine variable_swap(d,b,sigma,i,j)
   !swaps variables i and j of multivariate normal of dimension d and does appropitate changes in bounds b and covariance matrix sigma
      integer, intent(in) :: d, i, j
      real*8, dimension(d), intent(in out) :: b
      real*8, dimension(d,d), intent(in out) :: sigma
      real*8, dimension(1,d) :: temp_row
      real*8, dimension(d,1) :: temp_column
      real*8 :: temp_b

      temp_b = b(i)
      b(i) = b(j)
      b(j) = temp_b

      ! JE now exchanges rows i and row j of sigma
      temp_row(1,:) = sigma(i,:)
      sigma(i,:) = sigma(j,:)
      sigma(j,:) = temp_row(1,:)
      ! JE now exchanges column i and column j of sigma
      temp_column(:,1) = sigma(:,i)
      sigma(:,i) = sigma(:,j)
      sigma(:,j) = temp_column(:,1)

   end subroutine variable_swap

end function tvn_cdf_fast

function tvn_cdf_slow(b_in,corr_in) result(tvn_cdf)
   !evaluates the trivariate normal cdf with Genz's algorithm, which is slower since it uses adaptive integration
   real*8 :: tvn_cdf
   real*8, dimension(3),intent(in) :: b_in
   real*8, dimension(3,3),intent(in) :: corr_in

   real*8, dimension(3) ::  r
   integer :: i



   r(1) = corr_in(2,1)    !r(1) = corr(x1,x2)
   r(2) = corr_in(3,1)    !r(2) = corr(x1,x3)
   r(3) = corr_in(3,2)    !r(3) = corr(x2,x3)

   tvn_cdf= MVTVTL(0, b_in, r )


end function tvn_cdf_slow


function mvn_cdf_fast_less_args( m,b_in, c ) result(mvn_cdf)

    !evaluates rectangular multivariate normal probabiliities with Genz's 1992 algorithm, using quadrature based on
      !Smolyak's 1963 algorithm

   !references
   !Alan Genz: Numerical Computation of Multivariate Normal Probabilities, published in J. Comp. Graph Stat. 1 (1992), pp. 141-149
   !quadrature info from http://www.sparse-grids.de/

   !calculates probability P(x<b), x ~N(0,Sigma)
   !input need to be the cholesky decomposition

   real*8 :: mvn_cdf

   integer,intent(in) :: m !number of dimensions
   real*8, dimension(m), intent(in) :: b_in    !upper limits of integration
   real*8, dimension(m,m), intent(in) :: c  !c is the colesky decomp of the covarinace matrix


   integer :: i,j, n_nodes

   !following variables follow Genz's notation
   real*8, dimension(m) ::  b,  e
   real*8, dimension(m-1) :: w, y
   real*8 :: weight, cy_sum

 !  print *, "within mvn_genz_smolyak"

   mvn_cdf = 0
   b = b_in

   !initialize the first element of some of the key vectors
   e(1) = cdf_norm( b(1) / c(1,1) )

   n_nodes = sg_obj(m-1,acc_level_mvn) % n_nodes

   do j=1,n_nodes

      !get nodes and weight
      w(:m-1) = sg_obj(m-1,acc_level_mvn) % nodes(:m-1,j)
      weight = sg_obj(m-1,acc_level_mvn) % weights(j)


      !now for evaluating the genz integrand
      do i = 2,m
         y(i-1) = inv_cdf_norm(  w(i-1) * ( e(i-1)  ) )

         cy_sum = dot_product( c(i,:i-1),y(:i-1) )
         e(i)=cdf_norm( (b(i)- cy_sum )/c(i,i) )

        ! f(i) = ( e(i) ) * f(i-1)
      end do

    !  mvn_cdf = mvn_cdf + f(m)*weight
      mvn_cdf = mvn_cdf + product(e)*weight


   end do


end function mvn_cdf_fast_less_args




function mvn_cdf_fast_more_args( m, a_in, b_in, mu, c ) result(mvn_cdf)

   !Warning, I've only done a quick test of the function for m=2.  Since it's so similar to the previous algorithm, this should be fine
   !same as above routine, but takes in more arguments, such as a mean and a lower limit of integration

    !evaluates rectangular multivariate normal probabiliities with Genz's 1992 algorithm, using quadrature based on
      !Smolyak's 1963 algorithm

   !references
   !Alan Genz: Numerical Computation of Multivariate Normal Probabilities, published in J. Comp. Graph Stat. 1 (1992), pp. 141-149
   !quadrature info from http://www.sparse-grids.de/

   !calculates probability P(a<x<b), x ~N(mu,Sigma)
   !input need to be the cholesky decomposition

   real*8 :: mvn_cdf

   integer,intent(in) :: m !number of dimensions
   real*8, dimension(m), intent(in) :: a_in    !lower limits of integration
   real*8, dimension(m), intent(in) :: b_in    !upper limits of integration
   real*8, dimension(m), intent(in) :: mu !mean vector
   real*8, dimension(m,m), intent(in) :: c  !c is the colesky decomp of the covarinace matrix


   integer :: i,j, nodes

   !following variables follow Genz's notation
   real*8, dimension(m) :: a, b, d, e, f
   real*8, dimension(m-1) :: w, y
   real*8 :: weight, cy_sum

 !  print *, "within mvn_genz_smolyak"

   mvn_cdf = 0
   a = a_in - mu !in Genz's formula's the mean is 0, so converting problem to where this is the case
   b = b_in - mu

   !initialize the first element of some of the key vectors
   d(1) = cdf_norm( a(1) / c(1,1) )
   e(1) = cdf_norm( b(1) / c(1,1) )
   f(1) = e(1) - d(1)

   nodes = sg_obj(m,acc_level_mvn) % n_nodes

   do j=1,nodes

      !get nodes and weight
      w(:m-1) = sg_obj(m,acc_level_mvn) % nodes(:m-1,j)
      weight = sg_obj(m,acc_level_mvn) % weights(j)

      !now for evaluating the genz integrand
      do i = 2,m
         y(i-1) = inv_cdf_norm( d(i-1) + w(i-1) * ( e(i-1) -d(i-1) ) )

         cy_sum = dot_product( c(i,:i-1),y(:i-1) )
         d(i)=cdf_norm( (a(i)- cy_sum )/c(i,i) )
         e(i)=cdf_norm( (b(i)- cy_sum )/c(i,i) )

         f(i) = ( e(i) - d(i) ) * f(i-1)

      end do

      mvn_cdf = mvn_cdf + f(m)*weight

   end do


end function mvn_cdf_fast_more_args

function mvn_cdf_slow(N,UPPER, COVRNC ) result(mvn_cdf)
   !evaluates the multivariate normal CDF with Genz's algorithm that does adaptive quasi-monte carlo
   real*8 :: mvn_cdf

   !following 2 parameters control the accuracy of the algorithm
   real*8, parameter :: RELEPS  =0.0D+0
   real*8, parameter :: ABSEPS  =1.0D-5

   !Inputs
   INTEGER,                         INTENT(IN)  :: N
   REAL*8, DIMENSION(N),   INTENT(IN)  :: UPPER
   REAL*8, DIMENSION(N,N), INTENT(IN)  :: COVRNC

   !other variables used to call the Genz function
   integer, parameter :: NU = 0
   integer  :: M = 0
   integer, dimension(N) :: INFIN
   integer :: MAXPTS = 1000
   real*8, dimension(N) :: LOWER
   real*8, dimension(N) :: DELTA
   real*8, dimension(N,N) :: CONSTR
   INTEGER                          :: INFORM, NEVALS
   REAL*8                  :: ERROR, VALUE
   integer :: i

   MAXPTS = 5000*(N**4)
   DELTA = 0.0D+0
   LOWER = -1.0D+200
   INFIN = 0
   M = N
   CONSTR = 0.0D+0
   do i = 1, N
      CONSTR(i,i) = 1.0D+0
   end do

   CALL MVDIST( N, COVRNC, NU, M, LOWER, CONSTR, UPPER, INFIN, DELTA,   &
                      MAXPTS, ABSEPS, RELEPS, ERROR, VALUE, NEVALS, INFORM )



   mvn_cdf = VALUE

end function mvn_cdf_slow


function mvn_cdf_multi(d_in, b_in, corr_in, chol_in) result(cdf) !subroutine that calls other functions and subroutines for calculating the mvn cdf
   !this subroutine requires both the covarinace matrix and the cholesky decomposition to be inputs

   integer, intent(in) :: d_in !dimension of mvn cdf in quesiton
   real*8, dimension(d_in), intent(in) :: b_in   !upper limits of integration
   real*8, dimension(d_in, d_in), intent(in) :: corr_in !this is the whole covariance matrix
   real*8, dimension(d_in, d_in), intent(in) :: chol_in !this is the cholesky decomp
   real*8 :: cdf


   !determining whether to sort variables before smolyak integration

  ! print *, "within mvn"

   !determine which algorithm to use
   select case(d_in)
      case(:0)
         cdf = 1.0D+0
      case(1)
         cdf = cdf_norm( b_in(1) )
      case(2)
         cdf = bvn_cdf( b_in, corr_in(2,1) )
      case(3)
         if (mvn_fast_switch .eqv. .true. ) then
            cdf = tvn_cdf_fast(b_in,corr_in)
         else
            cdf = tvn_cdf_slow(b_in,corr_in)
         end if

      case(4:)
         if (mvn_fast_switch .eqv. .true. ) then
            cdf = mvn_cdf_fast(d_in,b_in,chol_in)
         else
            cdf = mvn_cdf_slow(d_in,b_in,corr_in)
         end if
   end select

end function mvn_cdf_multi


function mvn_cdf_freq_sim(d, bounds, cholesky, iter) result( mvn_cdf) !estimates the multivariate normal cdf with a simple frequency simulator

   integer, intent(in):: d
   real*8, dimension(d), intent(in) :: bounds !the upper limits of integration
   real*8, dimension(d,d), intent(in) :: cholesky !the lower cholesky decomp
   integer, intent(in) :: iter !number of iterations to perform
   real*8  :: mvn_cdf

   real*8, dimension(d) :: random_temp, random_normal
   integer :: temp_sum
   integer :: i,j

   temp_sum=0

   do i=1,iter

      do j=1,d
         random_temp(j)=rnorm()
      end do
      random_normal=matmul(cholesky,random_temp)
      if (all(random_normal .le. bounds)) then
         temp_sum=temp_sum+1
      end if
   end do

   mvn_cdf = dble(temp_sum)/dble(iter)

end function mvn_cdf_freq_sim


end module mvn_cdf_mod



module gamma_mod



!contains functions related to the gamma distribution.  I got these these subroutine from
!http://jblevins.org/mirror/amiller/r_gamma.f90

contains


!Jonathan Eggleston, added drawing from the exponential distribution if s=1

FUNCTION random_gamma(s, b, first) RESULT(fn_val)

! Adapted from Fortran 77 code from the book:
!     Dagpunar, J. 'Principles of random variate generation'
!     Clarendon Press, Oxford, 1988.   ISBN 0-19-852202-9

!     N.B. This version is in `double precision' and includes scaling

!     FUNCTION GENERATES A RANDOM GAMMA VARIATE.
!     CALLS EITHER random_gamma1 (S > 1.0)
!     OR random_exponential (S = 1.0)
!     OR random_gamma2 (S < 1.0).

!     S = SHAPE PARAMETER OF DISTRIBUTION (0 < REAL).
!     B = Scale parameter

IMPLICIT NONE
INTEGER, PARAMETER  :: dp = SELECTED_REAL_KIND(12, 60)

REAL (dp), INTENT(IN)  :: s, b
LOGICAL, INTENT(IN)    :: first
REAL (dp)              :: fn_val

! Local parameters
REAL (dp), PARAMETER  :: one = 1.0_dp, zero = 0.0_dp, zero_tol = 1.0D-14

IF (s <= zero) THEN
  WRITE(*, *) 'SHAPE PARAMETER VALUE MUST BE POSITIVE'
  STOP
END IF

if( abs(s-one)< zero_tol) then
   fn_val = random_exponential()
else IF (s >= one) THEN
  fn_val = random_gamma1(s, first)
ELSE IF (s < one) THEN
  fn_val = random_gamma2(s, first)
END IF

! Now scale the random variable
fn_val = b * fn_val
RETURN

CONTAINS


FUNCTION random_gamma1(s, first) RESULT(fn_val)

! Adapted from Fortran 77 code from the book:
!     Dagpunar, J. 'Principles of random variate generation'
!     Clarendon Press, Oxford, 1988.   ISBN 0-19-852202-9

! FUNCTION GENERATES A RANDOM VARIATE IN [0,INFINITY) FROM
! A GAMMA DISTRIBUTION WITH DENSITY PROPORTIONAL TO GAMMA**(S-1)*EXP(-GAMMA),
! BASED UPON BEST'S T DISTRIBUTION METHOD

!     S = SHAPE PARAMETER OF DISTRIBUTION
!          (1.0 < REAL)

REAL (dp), INTENT(IN)  :: s
LOGICAL, INTENT(IN)    :: first
REAL (dp)              :: fn_val

!     Local variables
REAL (dp)             :: d, r, g, f, x
REAL (dp), SAVE       :: b, h
REAL (dp), PARAMETER  :: sixty4 = 64.0_dp, three = 3.0_dp, pt75 = 0.75_dp,  &
                         two = 2.0_dp, half = 0.5_dp

IF (s <= one) THEN
  WRITE(*, *) 'IMPERMISSIBLE SHAPE PARAMETER VALUE'
  STOP
END IF

IF (first) THEN                        ! Initialization, if necessary
  b = s - one
  h = SQRT(three*s - pt75)
END IF

DO
  CALL RANDOM_NUMBER(r)
  g = r - r*r
  IF (g <= zero) CYCLE
  f = (r - half)*h/SQRT(g)
  x = b + f
  IF (x <= zero) CYCLE
  CALL RANDOM_NUMBER(r)
  d = sixty4*g*(r*g)**2
  IF (d <= zero) EXIT
  IF (d*x < x - two*f*f) EXIT
  IF (LOG(d) < two*(b*LOG(x/b) - f)) EXIT
END DO
fn_val = x

RETURN
END FUNCTION random_gamma1



FUNCTION random_gamma2(s, first) RESULT(fn_val)

! Adapted from Fortran 77 code from the book:
!     Dagpunar, J. 'Principles of random variate generation'
!     Clarendon Press, Oxford, 1988.   ISBN 0-19-852202-9

! FUNCTION GENERATES A RANDOM VARIATE IN [0,INFINITY) FROM
! A GAMMA DISTRIBUTION WITH DENSITY PROPORTIONAL TO
! GAMMA2**(S-1) * EXP(-GAMMA2),
! USING A SWITCHING METHOD.

!    S = SHAPE PARAMETER OF DISTRIBUTION
!          (REAL < 1.0)

REAL (dp), INTENT(IN)  :: s
LOGICAL, INTENT(IN)    :: first
REAL (dp)              :: fn_val

!     Local variables
REAL (dp)             :: r, x, w
REAL (dp), SAVE       :: a, p, c, uf, vr, d
REAL (dp), PARAMETER  :: vsmall = EPSILON(one)

IF (s <= zero .OR. s >= one) THEN
  WRITE(*, *) 'SHAPE PARAMETER VALUE OUTSIDE PERMITTED RANGE'
  STOP
END IF

IF (first) THEN                        ! Initialization, if necessary
  a = one - s
  p = a/(a + s*EXP(-a))
  IF (s < vsmall) THEN
    WRITE(*, *) 'SHAPE PARAMETER VALUE TOO SMALL'
    STOP
  END IF
  c = one/s
  uf = p*(vsmall/a)**s
  vr = one - vsmall
  d = a*LOG(a)
END IF

DO
  CALL RANDOM_NUMBER(r)
  IF (r >= vr) THEN
    CYCLE
  ELSE IF (r > p) THEN
    x = a - LOG((one - r)/(one - p))
    w = a*LOG(x)-d
  ELSE IF (r > uf) THEN
    x = a*(r/p)**c
    w = x
  ELSE
    fn_val = zero
    RETURN
  END IF

  CALL RANDOM_NUMBER(r)
  IF (one-r <= w .AND. r > zero) THEN
    IF (r*(w + one) >= one) CYCLE
    IF (-LOG(r) <= w) CYCLE
  END IF
  EXIT
END DO

fn_val = x
RETURN

END FUNCTION random_gamma2

END FUNCTION random_gamma


FUNCTION random_exponential() RESULT(ran_exp)

! Adapted from Fortran 77 code from the book:
!     Dagpunar, J. 'Principles of random variate generation'
!     Clarendon Press, Oxford, 1988.   ISBN 0-19-852202-9

! FUNCTION GENERATES A RANDOM VARIATE IN [0,INFINITY) FROM
! A NEGATIVE EXPONENTIAL DlSTRIBUTION WlTH DENSITY PROPORTIONAL
! TO EXP(-random_exponential), USING INVERSION.

IMPLICIT NONE
REAL  :: ran_exp

!     Local variable
REAL  :: r

DO
  CALL RANDOM_NUMBER(r)
  IF (r > 0) EXIT
END DO

ran_exp = -LOG(r)
RETURN

END FUNCTION random_exponential









end module gamma_mod



module chi_square_mod
!module  containing function used to compute chi_squared probabilities
!gotten from http://jblevins.org/mirror/amiller/chi_sq.f90

use lapack_mod
use gamma_mod

contains


FUNCTION chi_squared(ndf, chi2) RESULT(prob)
! Calculate the chi-squared distribution function
! ndf  = number of degrees of freedom
! chi2 = chi-squared value
! prob = probability of a chi-squared value <= chi2 (i.e. the left-hand
!        tail area)

IMPLICIT NONE
INTEGER, PARAMETER     :: dp = SELECTED_REAL_KIND(15, 60)
INTEGER, INTENT(IN)    :: ndf
REAL (dp), INTENT(IN)  :: chi2
REAL (dp)              :: prob



! Local variables
REAL (dp)  :: half = 0.5d0, x, p

x = half * chi2
p = half * REAL(ndf)
prob = gammad(x, p)
RETURN

END FUNCTION chi_squared


function random_chi_square(ndf)
   !gets a random chi_square with ndf degrees of freedom

   integer, intent(in) :: ndf
   real*8 :: random_chi_square

   random_chi_square = random_gamma( dble(ndf)/2, dble(2), .true. )


end function random_chi_square


!haven't tested the following two functions yet.

function chi_square_test_func(k,x,mu,sigma) result(prob)
   !does a chi-square test where (x-mu)'(sigma)^(-1)(x-mu) \sim \chi_square(k)
   integer, intent(in) :: k
   real*8, dimension(k),intent(in) :: x
   real*8, dimension(k),intent(in) :: mu
   real*8, dimension(k,k),intent(in) :: sigma
   real*8,dimension(1,1) :: chi2_value
   real*8 :: prob

   real*8, dimension(k,1) :: dif
   real*8, dimension(k,k) :: sigma_inv, sigma_chol

   !first get sigma_inverse
   call chol_decomp_sub(k,sigma,sigma_chol)
   call inv_chol_decomp_sub(k, sigma_chol, sigma_inv)

   dif(:,1) = x - mu

   chi2_value = matmul( matmul( transpose(dif), sigma_inv) , dif )
   prob = chi_squared(k,chi2_value(1,1))


end function chi_square_test_func


subroutine pearson_test_matrix_input(n_rows_in, n_cols_in, expect_in, obs_in, test_stat_out, pval_out )
   !does a pearson's chi-square test with the main inputes being in matrix form
   integer, intent(in) :: n_rows_in
   integer, intent(in) :: n_cols_in
   real*8, dimension(n_rows_in, n_cols_in), intent(in) :: expect_in
   real*8, dimension(n_rows_in, n_cols_in), intent(in) :: obs_in
   real*8, intent(out) :: test_stat_out
   real*8, intent(out) :: pval_out

   integer :: i, j

   test_stat_out = 0

   do i = 1, n_rows_in
      do j = 1, n_cols_in
         test_stat_out = test_stat_out + (expect_in(i,j) - obs_in(i,j))**2 / expect_in(i,j)
      end do
   end do


   pval_out = 1 - chi_squared(n_rows_in*n_cols_in - 1, test_stat_out)


end subroutine pearson_test_matrix_input


function mean_dif_test_func(k,mu_1,mu_2,sigma) result(prob)
   !does a chi_square test to see if two means of dimension k are the same
   integer, intent(in) :: k
   real*8, dimension(k),intent(in) :: mu_1
   real*8, dimension(k),intent(in) :: mu_2
   real*8, dimension(k,k),intent(in) :: sigma
   real*8 :: prob

   prob = chi_square_test_func(k,mu_1,mu_2,2*sigma)

end function mean_dif_test_func








FUNCTION gammad(x, p) RESULT(fn_val)

!   ALGORITHM AS239  APPL. STATIST. (1988) VOL. 37, NO. 3

!   Computation of the Incomplete Gamma Integral

!   Auxiliary functions required: lngamma = logarithm of the gamma
!   function, and ALNORM = algorithm AS66

! ELF90-compatible version by Alan Miller
! amiller@bigpond.net.au
! Latest revision - 28 October 2000

! N.B. Argument IFAULT has been removed

IMPLICIT NONE
INTEGER, PARAMETER     :: dp = SELECTED_REAL_KIND(12, 60)
REAL (dp), INTENT(IN)  :: x, p
REAL (dp)              :: fn_val



! Local variables
REAL (dp)             :: pn1, pn2, pn3, pn4, pn5, pn6, arg, c, rn, a, b, an
REAL (dp), PARAMETER  :: zero = 0.0_dp, one = 1.0_dp, two = 2.0_dp, &
                         oflo = 1.d+37, three = 3.0_dp, nine = 9.0_dp, &
                         tol = 1.d-14, xbig = 1.d+8, plimit = 1000.0_dp, &
                         elimit = -88.0_dp
! EXTERNAL lngamma, alnorm

fn_val = zero

!      Check that we have valid values for X and P

IF (p <= zero .OR. x < zero) THEN
  WRITE(*, *) 'AS239: Either p <= 0 or x < 0'
  RETURN
END IF
IF (x == zero) RETURN

!      Use a normal approximation if P > PLIMIT

IF (p > plimit) THEN
  pn1 = three * SQRT(p) * ((x / p) ** (one / three) + one /(nine * p) - one)
  fn_val = alnorm(pn1, .false.)
  RETURN
END IF

!      If X is extremely large compared to P then set fn_val = 1

IF (x > xbig) THEN
  fn_val = one
  RETURN
END IF

IF (x <= one .OR. x < p) THEN

!      Use Pearson's series expansion.
!      (Note that P is not large enough to force overflow in lngamma).
!      No need to test IFAULT on exit since P > 0.

  arg = p * LOG(x) - x - lngamma(p + one)
  c = one
  fn_val = one
  a = p
  DO
    a = a + one
    c = c * x / a
    fn_val = fn_val + c
    IF (c <= tol) EXIT
  END DO
  arg = arg + LOG(fn_val)
  fn_val = zero
  IF (arg >= elimit) fn_val = EXP(arg)

ELSE

!      Use a continued fraction expansion

  arg = p * LOG(x) - x - lngamma(p)
  a = one - p
  b = a + x + one
  c = zero
  pn1 = one
  pn2 = x
  pn3 = x + one
  pn4 = x * b
  fn_val = pn3 / pn4
  DO
    a = a + one
    b = b + two
    c = c + one
    an = a * c
    pn5 = b * pn3 - an * pn1
    pn6 = b * pn4 - an * pn2
    IF (ABS(pn6) > zero) THEN
      rn = pn5 / pn6
      IF (ABS(fn_val - rn) <= MIN(tol, tol * rn)) EXIT
      fn_val = rn
    END IF

    pn1 = pn3
    pn2 = pn4
    pn3 = pn5
    pn4 = pn6
    IF (ABS(pn5) >= oflo) THEN

!      Re-scale terms in continued fraction if terms are large

      pn1 = pn1 / oflo
      pn2 = pn2 / oflo
      pn3 = pn3 / oflo
      pn4 = pn4 / oflo
    END IF
  END DO

  arg = arg + LOG(fn_val)
  fn_val = one
  IF (arg >= elimit) fn_val = one - EXP(arg)
END IF


RETURN
END FUNCTION gammad



!  Algorithm AS66 Applied Statistics (1973) vol.22, no.3

!  Evaluates the tail area of the standardised normal curve
!  from x to infinity if upper is .true. or
!  from minus infinity to x if upper is .false.

! ELF90-compatible version by Alan Miller
! Latest revision - 29 November 2001

FUNCTION alnorm( x, upper ) RESULT( fn_val )
   IMPLICIT NONE
   INTEGER, PARAMETER    :: dp = SELECTED_REAL_KIND(15, 100)
   REAL(dp), INTENT(IN)  :: x
   LOGICAL, INTENT(IN)   :: upper
   REAL(dp)              :: fn_val

   !  Local variables
   REAL(dp), PARAMETER   ::  zero=0.0_dp, one=1.0_dp, half=0.5_dp, con=1.28_dp
   REAL(dp)              ::  z, y
   LOGICAL               ::  up

   !  Machine dependent constants
   REAL(dp), PARAMETER  :: ltone = 7.0_dp, utzero = 18.66_dp
   REAL(dp), PARAMETER  :: p = 0.398942280444_dp, q = 0.39990348504_dp,   &
                           r = 0.398942280385_dp, a1 = 5.75885480458_dp,  &
                           a2 = 2.62433121679_dp, a3 = 5.92885724438_dp,  &
                           b1 = -29.8213557807_dp, b2 = 48.6959930692_dp, &
                           c1 = -3.8052D-8, c2 = 3.98064794D-4,           &
                           c3 = -0.151679116635_dp, c4 = 4.8385912808_dp, &
                           c5 = 0.742380924027_dp, c6 = 3.99019417011_dp, &
                           d1 = 1.00000615302_dp, d2 = 1.98615381364_dp,  &
                           d3 = 5.29330324926_dp, d4 = -15.1508972451_dp, &
                           d5 = 30.789933034_dp

   up = upper
   z = x
   IF( z < zero ) THEN
      up = .NOT. up
      z = -z
   END IF
   IF( z <= ltone  .OR.  (up  .AND.  z <= utzero) ) THEN
      y = half*z*z
      IF( z > con ) THEN
         fn_val = r*EXP( -y )/(z+c1+d1/(z+c2+d2/(z+c3+d3/(z+c4+d4/(z+c5+d5/(z+c6))))))
      ELSE
         fn_val = half - z*(p-q*y/(y+a1+b1/(y+a2+b2/(y+a3))))
      END IF
   ELSE
      fn_val = zero
   END IF

   IF( .NOT. up ) fn_val = one - fn_val
   RETURN
END FUNCTION alnorm



FUNCTION lngamma(z) RESULT(lanczos)

!  Uses Lanczos-type approximation to ln(gamma) for z > 0.
!  Reference:
!       Lanczos, C. 'A precision approximation of the gamma
!               function', J. SIAM Numer. Anal., B, 1, 86-96, 1964.
!  Accuracy: About 14 significant digits except for small regions
!            in the vicinity of 1 and 2.

!  Programmer: Alan Miller
!              1 Creswick Street, Brighton, Vic. 3187, Australia
!  Latest revision - 14 October 1996

IMPLICIT NONE
INTEGER, PARAMETER    :: dp = SELECTED_REAL_KIND(15, 60)
REAL(dp), INTENT(IN)  :: z
REAL(dp)              :: lanczos

! Local variables

REAL(dp)  :: a(9) = (/ 0.9999999999995183D0, 676.5203681218835D0, &
                       -1259.139216722289D0, 771.3234287757674D0, &
                       -176.6150291498386D0, 12.50734324009056D0, &
                       -0.1385710331296526D0, 0.9934937113930748D-05, &
                        0.1659470187408462D-06 /), zero = 0.D0,   &
             one = 1.d0, lnsqrt2pi = 0.9189385332046727D0, &
             half = 0.5d0, sixpt5 = 6.5d0, seven = 7.d0, tmp
INTEGER   :: j

IF (z <= zero) THEN
  WRITE(*, *) 'Error: zero or -ve argument for lngamma'
  RETURN
END IF

lanczos = zero
tmp = z + seven
DO j = 9, 2, -1
  lanczos = lanczos + a(j)/tmp
  tmp = tmp - one
END DO
lanczos = lanczos + a(1)
lanczos = LOG(lanczos) + lnsqrt2pi - (z + sixpt5) + (z - half)*LOG(z + sixpt5)
RETURN

END FUNCTION lngamma



end module chi_square_mod


module wishart_mod
!module for drawing parameters from the wishart distribution

   !draws a random variable from the wishart distribution
   use lapack_mod
   use normal_mod
   use chi_square_mod


contains


function random_wishart_inv(k,df,v)
   !based on Code by Catherine Alford which is based on the algorithm outline in
   !1McCulloch, R., and P. Rossi, 1994, \An exact likelihood analysis of the multinomial probit model," Journal of Econometrics 64, 207-240.
   !also following the wishart stuff described on wikipedia

   !using McCulloch, R., and P. Rossi, 1994 notation

   integer, intent(in) :: k !dimension of v
   integer, intent(in) :: df !degrees of freedom
   real*8, dimension(k,k), intent(in) :: v !the input matrix
   real*8, dimension(k,k) :: random_wishart_inv

   real*8, dimension(k,k) :: inv_v, chol_inv_v, t, c, random_wishart
   integer :: i,j

   !get the colesky decomp of the inverse of v
   call inv_pd_matrix_sub(k, v, inv_v)
   call chol_decomp_sub(k, inv_v,chol_inv_v)

   !the matrix t is a lower traingle matrix with random chi_squares on the diagonal and random_normals on the off_diagonal
   t=0

   do i=1,k
      t(i,i) = sqrt(random_chi_square(df))
   end do

   do i=2,k
      do j=1,i-1
         t(i,j) = rnorm()
      end do
   end do

   c= matmul( chol_inv_v, t)
   random_wishart = matmul(c, transpose(c))
   call inv_pd_matrix_sub( k, random_wishart, random_wishart_inv)



end function random_wishart_inv

function random_sigma_bayesian(n,k,nu,x_transpose,psi)
   !draws a new random sigma assuming the prior for sigma is inv_wishart(nu,psi)
   !using wikipedia's notation at http://en.wikipedia.org/wiki/Conjugate_prior

   integer, intent(in) :: n !number of obs
   integer, intent(in) :: k !number of variables
   integer, intent(in) :: nu !prior degrees of freedom
   real*8, dimension(k,n), intent(in) :: x_transpose !data with zero mean
   real*8, dimension(k,k), intent(in) :: psi !input matrix
   real*8, dimension(k,k) :: random_sigma_bayesian

   real*8, dimension(n,k) :: x
   integer :: posterior_df
   real*8, dimension(k,k) :: posterior_matrix

   x = transpose(x_transpose)
   posterior_df = n + nu
   posterior_matrix = psi + matmul(x_transpose,x)

   random_sigma_bayesian = random_wishart_inv( k, posterior_df, posterior_matrix )

end function random_sigma_bayesian


end module wishart_mod





! Recursive Fortran 95 quicksort routine
! sorts real numbers into ascending numerical order
! Author: Juli Rew, SCD Consulting (juliana@ucar.edu), 9/03
! Based on algorithm from Cormen et al., Introduction to Algorithms,
! 1997 printing

! Made F conformant by Walt Brainerd

module qsort_c_module

implicit none


contains

recursive subroutine QsortC(A)
  real*8, intent(in out), dimension(:) :: A
  integer :: iq

  if(size(A) > 1) then
     call Partition(A, iq)
     call QsortC(A(:iq-1))
     call QsortC(A(iq:))
  endif

contains

subroutine Partition(A, marker)
  real*8, intent(in out), dimension(:) :: A
  integer, intent(out) :: marker
  integer :: i, j
  real*8 :: temp
  real*8 :: x      ! pivot point
  x = A(1)
  i= 0
  j= size(A) + 1

  do
     j = j-1
     do
        if (A(j) <= x) exit
        j = j-1
     end do
     i = i+1
     do
        if (A(i) >= x) exit
        i = i+1
     end do
     if (i < j) then
        ! exchange A(i) and A(j)
        temp = A(i)
        A(i) = A(j)
        A(j) = temp
     elseif (i == j) then
        marker = i+1
        return
     else
        marker = i
        return
     endif
  end do

end subroutine Partition

end subroutine QsortC

end module qsort_c_module


module stat_estimates_mod
   !contains various function for estimate function mean, sd, cov matrices, ect

   use qsort_c_module

   integer, parameter :: n_percent = 9
   real*8, dimension(n_percent), parameter :: percent_nodes = (/.01D+0, .05D+0, .1D+0, .25D+0, .5D+0, &
      .75D+0, .9D+0, .95D+0, .99D+0 /)

contains

function mean_scalar_est(n,x)
   !gets estimate of a scalar mean
   integer, intent(in) :: n
   real*8, dimension(n), intent(in) :: x
   real*8 :: mean_scalar_est

   mean_scalar_est = sum(x)/n

end function mean_scalar_est

function mean_vec_est(n,k,x)
   !gets estimate of a vector of means
   integer, intent(in) :: n
   integer, intent(in) :: k
   real*8, dimension(k,n), intent(in) :: x
   real*8,dimension(k) :: mean_vec_est
   integer :: i

   mean_vec_est = 0

   do i=1,n
      mean_vec_est(:) = mean_vec_est(:) + x(:,i)
   end do

   mean_vec_est = mean_vec_est / n

end function mean_vec_est

function mean_vec_weighted_est(n,k,x,w)
   !gets estimate of a vector of means
   integer, intent(in) :: n
   integer, intent(in) :: k
   real*8, dimension(k,n), intent(in) :: x
   real*8, dimension(n) :: w
   real*8,dimension(k) :: mean_vec_weighted_est
   integer :: i

   mean_vec_weighted_est = 0

   do i=1,n
      mean_vec_weighted_est(:) = mean_vec_weighted_est(:) + x(:,i)*w(i)
   end do

   mean_vec_weighted_est = mean_vec_weighted_est / sum(w)


end function mean_vec_weighted_est




function sd_scalar_est(n,x,mu)
   !gets estimate of a scalar standard deviation
   integer, intent(in) :: n
   real*8, dimension(n), intent(in) :: x
   real*8, intent(in) :: mu
   real*8 :: sd_scalar_est

   real*8, dimension(n) :: deviation_temp

   deviation_temp(:) = x(:) - mu

   sd_scalar_est = dot_product(deviation_temp,deviation_temp)/(n-1)
   sd_scalar_est = sqrt(sd_scalar_est)

end function sd_scalar_est



function cov_matrix_est(n,k,x,mu)
   !estimates a k by k covariance matrixx
   integer, intent(in) :: n !number of observations
   integer, intent(in) :: k !number of variables
   real*8, dimension(k,n), intent(in) :: x !data
   real*8, dimension(k) :: mu !mean of x
   real*8, dimension(k,k) :: cov_matrix_est

   integer :: i,j1,j2
   real*8, dimension(k) :: deviation_temp
   real*8, dimension(k,k) :: cov_temp

   cov_matrix_est = 0

   do i=1,n
      deviation_temp = x(:,i) - mu(:)

      do j1=1,k
         do j2=1,k
               cov_temp(j1,j2)=deviation_temp(j1)*deviation_temp(j2)
         end do
      end do

      cov_matrix_est = cov_matrix_est + cov_temp
   end do

   cov_matrix_est = cov_matrix_est /(n-1)

end function cov_matrix_est

function silverman_func(n, sigma)
   integer, intent(in) :: n
   real*8, intent(in) :: sigma
   real*8 :: silverman_func

   real*8, parameter :: silverman_factor = ((4.0/3.0)**.2)

   silverman_func = silverman_factor*sigma*(dble(n)**(-.2))

end function silverman_func

function percent_func(n, x)
   !gets percentiles
   integer, intent(in) :: n
   real*8, dimension(n), intent(in) :: x
   real*8, dimension(n_percent) :: percent_func

   real*8, dimension(n) :: x_sorted
   integer, dimension(n_percent) :: percent_index
   integer :: i

   x_sorted = x
   call QsortC(x_sorted)

   percent_index = nint(percent_nodes*n)
   percent_index = max(percent_index,1) !just in case n is small and the nearest nint is 0

   do i=1,n_percent
      percent_func(i) = x_sorted(percent_index(i))
   end do

end function percent_func




end module stat_estimates_mod
